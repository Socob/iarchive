#!/usr/bin/env python3
import argparse
import socket
import urls
import filter_db


ARCHIVE_URL = 'https://web.archive.org/save/'

# parse command line arguments
parser = argparse.ArgumentParser(description='Recursively crawl a URL and '
	'save all links to the Internet Archive')
parser.add_argument('--prefixes', nargs='+',
	help='Additional prefixes to crawl recursively (in addition to the '
		'initial URLs given in [URL])')
parser.add_argument('--delay', type=float, default=3.5,
	help='Delay between save operations in seconds (default: %(default)s)')
parser.add_argument('--retry', type=int, default=3,
	help='Number of times to retry if saving fails (default: %(default)d)')
parser.add_argument('--timeout', type=float, default=120,
	help='Timeout for blocking socket operations in seconds, see '
		'https://docs.python.org/3/library/'
		'socket.html#socket.setdefaulttimeout '
		'(default: %(default)d)')
parser.add_argument('--default-charset', default='utf-8',
	help='Default charset for HTML pages if unspecified (default: %(default)s)')
parser.add_argument('--user-agent',
	help='User-Agent HTTP header value to use for crawling')
parser.add_argument('--test', action='store_true',
	help='Do not actually save to Internet Archive')
parser.add_argument('URL', nargs='*',
	help='URLs to be saved (if empty: use URLs from url_list.py)')
args = parser.parse_args()

try:
	import url_list
	URLS = url_list.URLS
except ImportError:
	URLS = []


archive_saved_urls = set()


def process_rule(url_rule, url_queue):
	url_queue.force_add(url_rule)
	while url_queue:
		cur_url = url_queue.pop()
		# crawl for links if depth limit not reached
		if cur_url.depth > 0:
			urls.url_crawl(
				cur_url, url_queue, default_charset=args.default_charset,
				user_agent=args.user_agent
			)
		# save to archive
		save_url = f'{ARCHIVE_URL}{cur_url.url_str_coded}'
		if save_url in archive_saved_urls:
			# shouldn’t happen, but make sure to prevent multiple savings anyway
			print('SAVE: Skipping already-saved URL', save_url)
			assert False
		else:
			urls.archive_save_url(
				save_url, retry=args.retry, delay=args.delay, test=args.test
			)
			archive_saved_urls.add(save_url)

def main():
	socket.setdefaulttimeout(args.timeout)
	urls_todo = []
	if args.URL:
		urls_todo = [urls.URL(arg, prefixes=args.prefixes) for arg in args.URL]
	else:
		urls_todo = URLS
	for url in urls_todo:
		url.filters = url.filters + tuple(filter_db.FILTERS)
	url_queue = urls.URLQueue()
	while urls_todo:
		cur_rule = urls_todo.pop()
		process_rule(cur_rule, url_queue)

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print()

