#!/usr/bin/env python3
import copy
import math
import re
import socket
import string
import sys
import time
import urllib.parse
from html.parser import HTMLParser
from http.client import HTTPException, InvalidURL
from urllib.error import HTTPError, URLError
from urllib.parse import ParseResult, parse_qs, quote, unquote, urlencode, \
	urljoin, urlparse, urlunparse
from urllib.request import Request, urlopen

HTML_MIME_TYPES = ('text/html', 'application/xhtml+xml')
WAYBACK_ERROR_HEADER = 'X-Archive-Wayback-Runtime-Error'
QUOTE_SAFE = ''.join(set(string.printable) - {'%', ' '})


def unparse_qs(query_dict):
	return urllib.parse.unquote(urllib.parse.urlencode(
		query_dict, doseq=True, quote_via=urllib.parse.quote
	))


def PR(
	pr=None, scheme=None, netloc=None, path=None, params=None, query=None,
	fragment=None
):
	if pr is None:
		if scheme is None: scheme = ''
		if netloc is None: netloc = ''
		if path is None: path = ''
		if params is None: params = ''
		if query is None: query = ''
		if fragment is None: fragment = ''
		pr = (scheme, netloc, path, params, query, fragment)
	else:
		if not isinstance(pr, ParseResult):
			pr = urlparse(pr)
		pr = list(pr)
		if scheme is not None: pr[0] = scheme
		if netloc is not None: pr[1] = netloc
		if path is not None: pr[2] = path
		if params is not None: pr[3] = params
		if query is not None: pr[4] = query
		if fragment is not None: pr[5] = fragment
	return urlparse(urlunparse(pr))

class URL:
	DEPTH_DEFAULT = 100
	DEPTH_INF = math.inf

	# Note: The arguments “url”, “prefixes” and “exclude” are assumed to be
	#       URL-encoded! (Relevant if an URL contains actuale percent
	#       characters, i.e. %25.)
	def __init__(self, url, depth=DEPTH_DEFAULT, prefixes=None, exclude=set(),
		exclude_regex=None, as_prefix=True, filters=(), parent=None
	):
		self.pr = self.unquote_pr(PR(url))
		self.depth = depth
		self.filters = tuple(filters)
		# note: parent attribute is mostly for debugging purposes
		self.parent = parent
		self.exclude = {self.unquote_pr(PR(x)) for x in exclude}
		if exclude_regex is not None:
			self.exclude_regex = re.compile(exclude_regex)
		else:
			self.exclude_regex = None
		self.prefixes = set()
		# use domain as base unless otherwise specified
		if prefixes is None:
			if not as_prefix:
				u = self.pr
				self.prefixes = {
					self.unquote_pr(PR(scheme=u.scheme, netloc=u.netloc))
				}
		else:
			self.prefixes = {self.unquote_pr(PR(x)) for x in prefixes}
		if as_prefix:
			self.prefixes.add(self.pr)

	@property
	def pr_coded(self):
		pr = self.pr
		# URL-encode: puny-code hostname (IDNA) and percent-encode path
		netloc_coded = URL.netloc_encode(pr)
		path_coded = quote(pr.path, safe=QUOTE_SAFE)
		params_coded = quote(pr.params, safe=QUOTE_SAFE)
		# XXX
		# query string also needs to be percent-encoded, but encoding should
		# follow the encoding of its origin page
		query_coded = quote(pr.query, safe=QUOTE_SAFE)
		return PR(pr, netloc=netloc_coded, path=path_coded,
			params=params_coded, query=query_coded)

	@property
	def url_str(self):
		return self.pr.geturl()

	@property
	def url_str_coded(self):
		return self.pr_coded.geturl()

	def child_url(self, child, url_queue):
		child = self.unquote_pr(PR(child))
		res = copy.copy(self)
		res.pr = child
		# check exclusion criteria
		# excluded by exclude prefix (blacklist)?
		excluded = any([(child.hostname == ex.hostname and
			child.path.startswith(ex.path)) for ex in self.exclude])
		# excluded by regex?
		excluded |= (self.exclude_regex is not None
			and self.exclude_regex.search(self.url_str) is not None)
		# does any include prefix match (whitelist)?
		excluded |= not any([(child.hostname == parent.hostname and
			child.path.startswith(parent.path)) for parent in self.prefixes])
		# don’t crawl new URL (depth = 0) if it’s been excluded
		res.depth = 0 if excluded else self.depth - 1
		# apply passed filter
		for filt in self.filters:
			filt(res, url_queue)
		res.strip_session_ids()
		res.parent = self
		return res

	def __eq__(self, other):
		if isinstance(other, URL):
			return self.pr == other.pr
		else:
			return False

	def __hash__(self):
		return hash(self.pr)

	def __str__(self):
		return self.url_str

	def __repr__(self):
		return (
			'URL({!r}, depth={!r}, prefixes={!r}, exclude={!r}, '
			'exclude_regex={!r}, filters={!r})'
		).format(
			self.url_str, self.depth, self.prefixes, self.exclude,
			self.exclude_regex, self.filters
		)

	def strip_query_args(self, args):
		pr = self.pr
		if any(key in pr.query for key in args):
			qs = parse_qs(pr.query, keep_blank_values=True)
			for key in args:
				qs.pop(key, None)
			self.pr = PR(pr, query=unparse_qs(qs))

	def strip_session_ids(self):
		pr = self.pr
		if 'jsessionid=' in pr.params:
			self.pr = PR(pr, params='')
		self.strip_query_args(['PHPSESSID', 'sectok'])

	@staticmethod
	def unquote_pr(pr):
		# decode if hostname is represented in Punycode (IDNA)
		netloc = URL.netloc_decode(pr)
		# decode percent-encoding
		path = unquote(pr.path)
		params = unquote(pr.params)
		query = unquote(pr.query)
		# remove fragment
		return PR(
			pr, netloc=netloc, path=path, params=params, query=query,
			fragment=''
		)

	@staticmethod
	def netloc_code(pr, encoding):
		hostname = pr.hostname
		if hostname is None:
			hostname = ''
		netloc = ''
		if pr.username: netloc += pr.username
		if pr.password: netloc += ':' + pr.password
		if pr.username or pr.password: netloc += '@'
		# encoding == 'idna': decode puny-coded hostname to Unicode string
		# encoding == 'ascii': get Punycode representation of hostname
		netloc += str(bytes(hostname, encoding='idna'), encoding=encoding)
		if pr.port: netloc += ':' + str(pr.port)
		return netloc

	@staticmethod
	def netloc_encode(pr):
		return URL.netloc_code(pr, 'ascii')

	@staticmethod
	def netloc_decode(pr):
		return URL.netloc_code(pr, 'idna')

class URLQueue:
	# TODO: choose highest depth when URL is already in queue
	def __init__(self):
		self._queue = set()
		self._done = set()

	def __bool__(self):
		return bool(self._queue)

	def add(self, url):
		if url not in self._done:
			self._queue.add(url)
		self._done.add(url)

	def force_add(self, url):
		self._queue.add(url)
		self._done.add(url)

	def discard(self, url):
		self._queue.discard(url)
		self._done.add(url)

	def update(self, urls):
		self._queue |= urls - self._done
		self._done |= urls

	def pop(self):
		res = self._queue.pop()
		assert res in self._done
		return res

class HTMLLinkParser(HTMLParser):
	def __init__(self, base, url_queue):
		super().__init__()
		self.urls = set()
		self.base = base
		self.url_queue = url_queue

	def handle_starttag(self, tag, attrs):
		attrs = dict(attrs)
		href = attrs.get('href')
		if href:
			try:
				if tag == 'base':
					new_base = copy.copy(self.base)
					# use urljoin to e.g. resolve protocol-relative URLs
					new_url = urljoin(self.base.url_str_coded, href)
					new_base.pr = new_base.unquote_pr(PR(new_url))
					self.base = new_base
				elif tag == 'a':
					h_lower = href.lower()
					if not (h_lower.startswith('javascript:')
						or h_lower.startswith('mailto:')
						or h_lower.startswith('tel:')
					):
						new_url = urljoin(self.base.url_str_coded, href)
						child = self.base.child_url(new_url, self.url_queue)
						self.urls.add(child)
			except ValueError:
				# invalid port number or IP
				pass

def url_crawl(cur_url, url_queue, default_charset='utf-8', user_agent=None):
	crawl_url = cur_url
	html = None
	url_encoded = cur_url.url_str_coded
	headers = {}
	if user_agent:
		headers['User-Agent'] = user_agent
	request = Request(url_encoded, headers=headers)
	try:
		with urlopen(request) as result:
			# URL might be redirected → get final destination
			red_url_str = result.geturl()
			headers = result.info()
			if headers.get_content_type() in HTML_MIME_TYPES:
				charset = headers.get_param('charset')
				if not charset:
					charset = default_charset
				html_bytes = result.read()
				# XXX if no charset information in HTTP header,
				# try parsing as ASCII to get encoding from HTML meta tag
				try:
					html = str(html_bytes, encoding=charset, errors='replace')
				except LookupError:
					print(
						f'CRAWL: Charset “{charset}” unknown, using '
						f'“{default_charset}”'
					)
					html = str(
						html_bytes, encoding=default_charset, errors='replace'
					)
		if cur_url.url_str != red_url_str:
			crawl_url = cur_url.child_url(red_url_str, url_queue)
			url_queue.discard(crawl_url)
		# check possibly changed depth if redirected
		if crawl_url and crawl_url.depth > 0 and html:
			parser = HTMLLinkParser(crawl_url, url_queue)
			parser.feed(html)
			parser.close()
			url_queue.update(parser.urls)
	# XXX still crawl HTML even if HTTP status was 404
	except (URLError, HTTPException, ConnectionError, socket.timeout) as e:
		print('CRAWL: Error opening URL', url_encoded, '–', e, file=sys.stderr)

def archive_save_url(save_url, retry=3, delay=3.5, test=False):
	retry_tot = retry
	while retry:
		retry -= 1
		if test:
			print('SAVE/TEST: Would open', save_url)
		else:
			print('SAVE: Opening', save_url)
		try:
			if not test:
				with urlopen(save_url): pass
			time.sleep(delay)
			retry = 0
		except (URLError, HTTPException, ConnectionError, socket.timeout) as e:
			error_code = None
			error_header = None
			if isinstance(e, HTTPError):
				error_header = e.headers.get(WAYBACK_ERROR_HEADER)
				error_code = e.code
			if not isinstance(e, HTTPError) or (
				isinstance(e, HTTPError) and e.code in range(500, 600)
			):
				print(
					'SAVE: Error saving using', save_url, '–', e, type(e),
					file=sys.stderr
				)
#				if error_code: print('HTTP', error_code, file=sys.stderr)
				if error_header: print(
					WAYBACK_ERROR_HEADER, '=', error_header, file=sys.stderr
				)
				if retry: print(f'Retrying… ({retry_tot - retry}/{retry_tot})')
				time.sleep(2 * delay)
			else:
				retry = 0

