import copy
import re
import urllib.parse
import urls


# Filters are called during the construction of child URLs (i.e. in
# URL.child_url()), so it’s safe to mutate them in filters

def cds_cern_ch_filter(url, url_queue):
	pr = url.pr
	if pr.hostname == 'cds.cern.ch':
		url.strip_query_args(['ln'])

def wwu_filter(url, url_queue):
	pr = url.pr
	if pr.scheme == 'http' and pr.hostname in (
		'www.uni-muenster.de', 'www.wwu.de', 'www.uni-ms.de',
		'www.wwu-muenster.de', 'wwwmath.uni-muenster.de'
	):
		url.pr = urls.PR(pr, scheme='https')

def index_filter(url, url_queue):
	pr = url.pr
	if pr.path.endswith('index.html'):
		url_no_index = copy.copy(url)
		url_no_index.pr = urls.PR(pr, path=pr.path[:-len('index.html')])
		url_queue.add(url_no_index)

def remove_replytocom(url, url_queue):
	pr = url.pr
	if 'replytocom=' in pr.query:
		url.pr = urls.PR(pr, query='')


FILTERS = [
	cds_cern_ch_filter,
	wwu_filter,
	index_filter,
]

