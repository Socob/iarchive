#!/bin/sh
date_fmt=%FT%T
now=$(date +$date_fmt)
python3 -u ./iarchive.py "$@" 2>&1 \
	| awk '{ print strftime("['$date_fmt']"), $0; fflush(); }' \
	| tee "logs/$now.txt"

